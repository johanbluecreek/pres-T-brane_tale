# There and back again: A T-brane's tale

## Abstract

In some particular backgrounds the non-Abelian dynamics of a stack of D*p*-branes can be described by an Abelian polarised D*(p+2)*-brane with worldvolume flux.  By investigating the defining equations for T-branes, we find a new "polarised" state for a stack of non-Abelian D*p*-branes where the "polarised" frame is a single Abelian D*p*-brane where the original T-branes data is encoded as worldvolume curvature. We will join an explicit T-brane construction's journey through T-dualities and brane polarisation to reach this new Abelian T-brane description.

## Information

This presentation is based on the paper:

* [arXiv:1608.01221](https://arxiv.org/abs/1608.01221)

This presentation was/will be given at:

* Theoretical Physics division, Uppsala University, Uppsala, Sweden @ 2016-10-19
* Institut de Physique Nucleaire de Lyon, Lyon, France @ 2016-11-14
